# @author Michael Hoglan
# Project source directory; this must be at the top before other includes
ROOT_PATH													:= $(shell dirname $(abspath $(lastword $(MAKEFILE_LIST))))
APPLICATIONS_COMMON_ROOT_PATH			:= $(shell dirname $(realpath $(abspath $(lastword $(MAKEFILE_LIST)))))
# Using simple expanded variables := when possible to ensure values stay as expected
APPLICATIONS_COMMON_SCRIPT_PATH		:= $(APPLICATIONS_COMMON_ROOT_PATH)/scripts

-include Makefile.vars
-include Makefile.vars.local
-include Makefile.project

# Set defaults; include Makefile based on PROJECT_TYPE
PROJECT_BUILD_UTILS_PATH					?= .build-utils
PROJECT_TYPE											?= none
include $(PROJECT_BUILD_UTILS_PATH)/applications/$(PROJECT_TYPE)/Makefile

# Define the output for usage which is printed by default
define INFO_BODY
{
  "project": {
    "name": "$(NAME)",
    "type": "$(PROJECT_TYPE)",
    "repository": "$(REPOSITORY)",
    "image_host": "$(IMAGE_HOST)",
    "image_name": "$(IMAGE_NAME)",
    "image_tag": "$(IMAGE_TAG)",
    "commit": "$(PROJECT_GIT_SHA)",
    "commit_iso8601": "$(PROJECT_GIT_COMMIT_ISO8601)"
  }
}
endef
export INFO_BODY

define USAGE_BODY
Valid targets: all clean clean-deps prep build test build-image push-image

Convenience targets: info list-images clean-images

`make docker-env` will jump into a container of build image
endef

# Set default goal if no target is specified
.DEFAULT_GOAL											:= default

# Default to 0 if no BUILD_NUMBER is passed in; usually means local development
PROJECT_BUILD_NUMBER							:= $(if $(BUILD_NUMBER),$(BUILD_NUMBER),0)
# TODO: get rid of this circleism;
# CI pipelines should specify BUILD_NUMBER as an ENV or commandline var to make
PROJECT_BUILD_NUMBER							:= $(if $(CIRCLE_BUILD_NUM),$(CIRCLE_BUILD_NUM),$(PROJECT_BUILD_NUMBER))
PROJECT_BUILD_DATE								:= $(shell date -u +"%Y%m%d.%H%M%S")
PROJECT_BUILD_DATE_3339						:= $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
PROJECT_GIT_NAME									:= $(shell basename $(shell git config --get remote.origin.url 2>/dev/null || echo $(ROOT_PATH)) .git)
PROJECT_GIT_SHA										:= $(shell git rev-parse HEAD)
PROJECT_GIT_SHA_SHORT							:= $(shell git rev-parse --short HEAD)
PROJECT_GIT_TOPIC_BRANCH					:= $(shell git rev-parse --abbrev-ref=strict HEAD)
PROJECT_GIT_COMMIT_ISO8601 				:= $(shell git log --pretty=format:"%ad" -1 --date=iso | sed -E 's/ /T/; s/ //g;')
PROJECT_GIT_MAINLINE_BRANCH				?= master
PROJECT_GIT_STAGING_BRANCH				?= develop
PROJECT_GIT_HOTFIX_BRANCH					?= hotfix


# Generate version information using most recent git tag version and commit counts
# <version-tag>-<mainline-commits>[-<topic-label>][-<topic-commits>]

# Get the tag representing version from mainline, only search parent; default to v0
PROJECT_GIT_VERSION_TAG						:= $(shell git describe --tags --first-parent --match v[0-9]* --abbrev=0 $(PROJECT_GIT_MAINLINE_BRANCH) 2>/dev/null || echo v0)

# Determine commit when topic branched off mainline
# Count number of commits on topic since branchpoint
# Count number of commits on mainline since version tag
PROJECT_GIT_BRANCHPOINT						:= $(shell git merge-base HEAD $(PROJECT_GIT_MAINLINE_BRANCH))
PROJECT_GIT_TOPIC_COMMITS					:= $(shell git rev-list $(PROJECT_GIT_BRANCHPOINT)..HEAD --count --first-parent 2>/dev/null || echo 0)
PROJECT_GIT_MAINLINE_COMMITS			:= $(shell git rev-list $(PROJECT_GIT_VERSION_TAG)..$(PROJECT_GIT_BRANCHPOINT) --count --first-parent 2>/dev/null || echo 0)

# Check if workspace is dirty with untracked / uncommitted changes
PROJECT_GIT_WORKSPACE_DIRTY				:= $(shell [ -z "$$(git status --porcelain)" ] || echo -dirty)
PROJECT_GIT_WORKSPACE_UNPUSHED		:= $(shell $$(git cherry 2>/dev/null) || echo -unpushed)

# Use SHA for label prefix;  Specific topics can have defined labels instead;
PROJECT_GIT_TOPIC_LABEL						:= $(PROJECT_GIT_SHA_SHORT)
PROJECT_GIT_TOPIC_LABEL						:= $(if $(filter $(PROJECT_GIT_MAINLINE_BRANCH),$(PROJECT_GIT_TOPIC_BRANCH)),$(PROJECT_GIT_MAINLINE_BRANCH),$(PROJECT_GIT_TOPIC_LABEL))
PROJECT_GIT_TOPIC_LABEL						:= $(if $(filter $(PROJECT_GIT_STAGING_BRANCH),$(PROJECT_GIT_TOPIC_BRANCH)),$(PROJECT_GIT_STAGING_BRANCH),$(PROJECT_GIT_TOPIC_LABEL))
PROJECT_GIT_TOPIC_LABEL						:= $(if $(filter $(PROJECT_GIT_HOTFIX_BRANCH),$(PROJECT_GIT_TOPIC_BRANCH)),$(PROJECT_GIT_HOTFIX_BRANCH),$(PROJECT_GIT_TOPIC_LABEL))

# Project version;  append topic label and commits if not mainline
PROJECT_VERSION										:= $(PROJECT_GIT_VERSION_TAG)-$(PROJECT_GIT_MAINLINE_COMMITS)
PROJECT_VERSION										:= $(if $(filter $(PROJECT_GIT_MAINLINE_BRANCH),$(PROJECT_GIT_TOPIC_BRANCH)),$(PROJECT_VERSION)-$(PROJECT_BUILD_NUMBER),$(PROJECT_VERSION)-$(PROJECT_GIT_TOPIC_LABEL)-$(PROJECT_GIT_TOPIC_COMMITS)-$(PROJECT_BUILD_NUMBER))

# Build label to be used for artifacts; includes build number which may be passed in;
PROJECT_BUILD_LABEL								:= $(PROJECT_VERSION)$(PROJECT_GIT_WORKSPACE_DIRTY)$(PROJECT_GIT_WORKSPACE_UNPUSHED)
PROJECT_BUILD_DATE_LABEL					:= $(PROJECT_VERSION)$(PROJECT_GIT_WORKSPACE_DIRTY)$(PROJECT_GIT_WORKSPACE_UNPUSHED)-$(PROJECT_BUILD_DATE)

# Check type of shell environment
INTERACTIVE_SHELL_PRESENT					:= $(shell [ -t 0 ] && echo true)

# Variables for building project inside container
BUILD_IMAGE												?= $($(shell echo $(PROJECT_TYPE) | tr '[a-z]' '[A-Z]')_BUILDER_IMAGE):$(BUILDER_IMAGE_VERSION)
BUILD_IMAGE_BASE_PATH							:= /opt/rewardStyle
BUILD_IMAGE_PROJECT_PATH					:= $(BUILD_IMAGE_BASE_PATH)/src/github.com/rewardStyle/$(NAME)
BUILD_IMAGE_PROJECT_MOUNT					:= $(ROOT_PATH):$(BUILD_IMAGE_PROJECT_PATH)
BUILD_IMAGE_WORKDIRECTORY					:= $(BUILD_IMAGE_PROJECT_PATH)
BUILD_IMAGE_VOLUME_MOUNTS					+=
BUILD_IMAGE_ENV_VARS							+=

# Vars from commandline; pass long to make calls in containers
MAKE_COMMANDLINE_VARS							:= $(strip $(foreach v,$(.VARIABLES),$(if $(filter command line,$(origin $(v))),$(v)=$(value $(value v)))))

# Variables for docker to use build image
# Set whether container has an interactive shell
DOCKER_CONTAINER_INTERACTIVE			:= $(if $(filter true, $(INTERACTIVE_SHELL_PRESENT)),-i,)
# Build list of volume mounts from generic build image volume mounts
DOCKER_VOLUME_MOUNTS							+= $(BUILD_IMAGE_VOLUME_MOUNTS:%=-v %)
# Add dependency cache mounts
DOCKER_VOLUME_MOUNTS							+= $(DEPENDENCY_CACHE_VOLUME_MOUNTS:%=-v %)
# Add project mount
DOCKER_VOLUME_MOUNTS							+= -v $(BUILD_IMAGE_PROJECT_MOUNT)
# Add project env vars for image
DOCKER_ENV_VARS										+= $(BUILD_IMAGE_ENV_VARS:%=-e %)
# Compose docker run command from vars
DOCKER_CONTAINER									:= docker run $(DOCKER_CONTAINER_INTERACTIVE) -t --rm \
																		--entrypoint /bin/sh \
																		$(DOCKER_ENV_VARS) \
																		$(DOCKER_VOLUME_MOUNTS) \
																		-w $(BUILD_IMAGE_WORKDIRECTORY) \
																		$(BUILD_IMAGE)
# Space separated list of container names or ids to wait upon
DOCKER_WAIT_CONTAINERS						?=
# Time in seconds per container to wait for it to become healthy
DOCKER_WAIT_TIMEOUT								?= 120
DOCKER_WAIT_CONTAINERS_HEALTHY		?= TIMEOUT=$(DOCKER_WAIT_TIMEOUT) $(APPLICATIONS_COMMON_SCRIPT_PATH)/wait-containers-healthy.sh $(DOCKER_WAIT_CONTAINERS)

# Variables for releasing image
RELEASE_IMAGE_ENVIRONMENTS				:= latest dev prod
RELEASE_IMAGE_TARGETS							:= $(RELEASE_IMAGE_ENVIRONMENTS:%=docker-build-image_%)
RELEASE_PUSH_TARGETS							:= $(RELEASE_IMAGE_ENVIRONMENTS:%=docker-push-image_%)
RELEASE_IMAGE_BUILD_OPTS					:= --force-rm

# temporary var to suffice Circle 1.0 needing to docker tag -f
RELEASE_IMAGE_TAG_OPTS						?=

# Image information
# Conditionally set if not already set (i.e. defaults)
IMAGE_REPOSITORY									?= $(IMAGE_HOST)/$(IMAGE_NAME)
IMAGE_DOCKERFILE									?= ./Dockerfile
IMAGE_CONTEXT											?= .
IMAGE_TAG													?= latest
IMAGE_UNIQUE_TAG									?= $(PROJECT_BUILD_DATE)
IMAGE_LABELS											:= --label=org.label-schema.name="$(NAME)" \
																			--label=org.label-schema.url="https://www.rewardstyle.com/" \
																			--label=org.label-schema.vcs-url="$(REPOSITORY)" \
																			--label=org.label-schema.vcs-ref="$(PROJECT_GIT_SHA)" \
																			--label=org.label-schema.vendor="rewardStyle" \
																			--label=org.label-schema.version="$(patsubst %-latest,%,$(PROJECT_BUILD_LABEL)-$(IMAGE_TAG))" \
																			--label=org.label-schema.build-date="$(PROJECT_BUILD_DATE_3339)" \
																			--label=org.label-schema.schema-version="1.0"

# TODO: Get this moved to a sub makefile / script that is optionally included.
# ECR access information
ECR_HOST													?= $(IMAGE_HOST)
ECR_REGISTRY_ID										?= $(firstword $(subst ., ,$(IMAGE_HOST)))
ECR_EXTRA_REGISTRY_IDS								?= $(EXTRA_REGISTRY_IDS)
ECR_GET_LOGIN										:= aws ecr --profile=${AWS_PROFILE} get-login --no-include-email --registry-ids $(ECR_REGISTRY_ID) $(ECR_EXTRA_REGISTRY_IDS)

# When running on circleci apply workarounds
ifneq ($(CIRCLE_BUILD_NUM),)
	# circleci does not support container deletion
	RELEASE_IMAGE_BUILD_OPTS				:= $(filter-out --force-rm,$(RELEASE_IMAGE_BUILD_OPTS))
	# circleci does not support image deletion
	DOCKER_CONTAINER								:= $(filter-out --rm,$(DOCKER_CONTAINER))
	# circleci does not support --labels flag on build command
	IMAGE_LABELS										:=
endif

# Define a docker/config.json body that can be outputted for setting ECR Credential Helper
# hardcoded to the rewardStyle ECR repository
define ECR_DOCKER_CONFIG_BODY
{
  "credHelpers" : {
    "770543094243.dkr.ecr.us-east-1.amazonaws.com" : "ecr-login"
	}
}
endef

# output info and usage by default
# use $(info ...) for output to avoid interpretation in shell
# first target is the default
.PHONY: default
default														:
																	@: $(info $(INFO_BODY))
																	@: $(info )
																	@: $(info $(USAGE_BODY))

# convenience targets for info, usage and debugging var values
.PHONY: info
info															:
																	@: $(info $(INFO_BODY))

.PHONY: usage
usage															:
																	@: $(info $(USAGE_BODY))

print-%														:
																	@echo '$*=$($*)'

# output version information for project
.PHONY: version
version														:
																	@echo 'PROJECT_VERSION=$(PROJECT_VERSION)'

# output ECR Credential Helper config 
.PHONY: ecr-docker-config
ecr-docker-config									:
																	@: $(info $(ECR_DOCKER_CONFIG_BODY))

.PHONY: all
all																: clean prep build test build-image

# shortcut targets to specific target types
# shortcutting to docker types to perform actions in a container
.PHONY: clean
clean															: docker-clean

.PHONY: clean-deps
clean-deps												: docker-clean-deps

.PHONY: prep
prep															: docker-prep

.PHONY: check
check															: docker-check

.PHONY: build
build															: docker-build

.PHONY: push-artifacts
push-artifacts										: docker-push-artifacts

# shortcut test to build
# presence of test target sets variable for build to run with tests
.PHONY: test
test															: docker-test

.PHONY: translations
translations											: docker-translations

.PHONY: validate-messages
validate-translations							: docker-validate-translations

.PHONY: extract-messages
extract-messages									: docker-extract-messages

.PHONY: push-messages
push-messages											: docker-push-messages

.PHONY: pull-messages
pull-messages											: docker-pull-messages

.PHONY: compile-messages
compile-messages									: docker-compile-messages

# shortcut to target defined by variable which may be empty (thus skipped)
# allows projects which do not need images built to no-op target
# @true will suppress "make: Nothing to be done for target" when empty
.PHONY: build-image
build-image												: $(if $(filter true, $(BUILD_IMAGE_ENABLED)),docker-build-image,)
																	@true

.PHONY: push-image
push-image												: $(if $(filter true, $(BUILD_IMAGE_ENABLED)),docker-push-image,)
																	@true

# convenience targets
# jump into a container build environment
.PHONY: docker-env
docker-env												:
																	$(DOCKER_CONTAINER) -c "/bin/sh"

.PHONY: list-images
list-images												:
																	docker image ls $(IMAGE_REPOSITORY)

.PHONY: clean-images
clean-images											:
																	docker image rm -f $$(docker image ls -q $(IMAGE_REPOSITORY) | sort | uniq)

# docker based targets
.PHONY: docker-clean
docker-clean											:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-clean"

.PHONY: docker-clean-deps
docker-clean-deps									:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-clean-deps"

.PHONY: docker-prep
docker-prep												:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-prep"

.PHONY: docker-check
docker-check											:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-check"

.PHONY: docker-build
docker-build											:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-build"

.PHONY: docker-test
docker-test												:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-test"

.PHONY: docker-push-artifacts
docker-push-artifacts												:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-push-artifacts"

.PHONY: docker-build-image
docker-build-image								:
																	docker build $(RELEASE_IMAGE_BUILD_OPTS) \
																		--tag "$(IMAGE_REPOSITORY):$(IMAGE_TAG)" \
																		$(IMAGE_LABELS) \
																		-f "$(IMAGE_DOCKERFILE)" "$(IMAGE_CONTEXT)"
																	docker tag $(RELEASE_IMAGE_TAG_OPTS) $(IMAGE_REPOSITORY):$(IMAGE_TAG) $(IMAGE_REPOSITORY):$(PROJECT_BUILD_LABEL)
																	docker tag $(RELEASE_IMAGE_TAG_OPTS) $(IMAGE_REPOSITORY):$(IMAGE_TAG) $(IMAGE_REPOSITORY):$(PROJECT_VERSION)
																	docker tag $(RELEASE_IMAGE_TAG_OPTS) $(IMAGE_REPOSITORY):$(IMAGE_TAG) $(IMAGE_REPOSITORY):$(PROJECT_GIT_SHA)

.PHONY: docker-push-image
docker-push-image									:
																	IMAGE_REPOSITORY="$(IMAGE_REPOSITORY)" \
																		IMAGE_TAG="$(IMAGE_TAG)" \
																		PROJECT_GIT_MAINLINE_BRANCH="$(PROJECT_GIT_MAINLINE_BRANCH)" \
																		PROJECT_GIT_NAME="$(PROJECT_GIT_NAME)" \
																		PROJECT_GIT_SHA="$(PROJECT_GIT_SHA)" \
																		PROJECT_GIT_TOPIC_BRANCH="$(PROJECT_GIT_TOPIC_BRANCH)" \
																		\
																		$(APPLICATIONS_COMMON_SCRIPT_PATH)/push-images.sh

.PHONY: docker-translations
docker-translations								:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-translations"

.PHONY: docker-validate-messages
docker-validate-translations			:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-validate-translations"

.PHONY: docker-extract-messages
docker-extract-messages						:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-extract-messages"

.PHONY: docker-push-messages
docker-push-messages							:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-push-messages"

.PHONY: docker-pull-messages
docker-pull-messages							:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-pull-messages"

.PHONY: docker-compile-messages
docker-compile-messages						:
																	$(DOCKER_CONTAINER) -c "make $(MAKE_COMMANDLINE_VARS) $(PROJECT_TYPE)-compile-messages"

.PHONY: ecr-login
ecr-login													:
																	@echo Logging into $(ECR_HOST) ...
																	@echo Output: $(shell $(ECR_GET_LOGIN) | while read -r cmd; do yes 'none' | $$cmd; done)

.PHONY: ecr-logout
ecr-logout												:
																	docker logout https://$(ECR_HOST)

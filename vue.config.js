/* eslint-disable @typescript-eslint/no-var-requires */
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require('path');

module.exports = {
    devServer: {
        open: process.platform === 'darwin',
        host: '0.0.0.0',
        port: 9999, // CHANGE YOUR PORT HERE!
        https: true,
        hotOnly: false,
        historyApiFallback: {
            rewrites: [
                { from: /^\/iframe\/?.*/, to: path.posix.join('/', 'iframe.html') },
                { from: /./, to: path.posix.join('/', 'index.html') }
            ]
        },
    },
    assetsDir: "static",
    pages: {
        app: { entry: "./src/main.js", filename: "index.html" },
        iframe: { entry: "./src/mainIframe.js", filename: "iframe.html" }
    },
    // transpileDependencies: ["vuetify", "vuex-module-decorators"],
    parallel: process.env.CI !== "true" && require("os").cpus().length > 1,
    chainWebpack: config => {
        config.plugins.delete("pwa");
        config.resolve.alias.set("package.json", __dirname + "/package.json");

        // Hacking the config files into the dev servers, either for E2E, or local development
        const { ENV } = process.env;
        if (ENV !== undefined) {
            config.plugin("copy-config").use(
                new CopyWebpackPlugin({
                    patterns: [
                        { from: `./src/config/${ENV}.js`, to: "static/js/config.js" }
                    ]
                })
            );
        } else {
            // Copies all config files over. Should really only be used when building for distribution
            const from = "./src/config";
            const to = "static/js/config/[name].js";
            config.plugin("copy-config").use(
                new CopyWebpackPlugin({
                    patterns: [{ from, to }]
                })
            );
        }
    }
};
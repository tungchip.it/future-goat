import {
    AUTH_REQUEST,
    AUTH_ERROR,
    AUTH_SUCCESS,
    AUTH_LOGOUT,
    AUTH_INIT,
    AUTH_INFLUENCER_SET,
    AUTH_UPDATE_TOKEN
} from "../actions/auth";
import Repository from "@/repositories/RepositoryFactory";

const riaServiceEnpoint = Repository.get("riaServiceEnpoint");

const state = {
    token: localStorage.getItem("userToken") || "",
    status: "",
    hasLoadedOnce: false,
    influencerId: "",
    influencerType: "",
    fullName: "",
    email: "", //email
    userType: "", // get after login
    availableInfluencers: [], // get after login
    profileImageUrl: ""
};

const getters = {
    isAuthenticated: state => !!state.token, // cast to boolean
    authStatus: state => state.status,
    influencerId: state => state.influencerId,
    influencerType: state => state.influencerType,
    fullName: state => state.fullName,
    email: state => state.email,
    userType: state => state.userType,
    availableInfluencers: state => state.availableInfluencers,
    profileImageUrl: state => state.profileImageUrl
};

const actions = {
    [AUTH_INIT]: context => {
        return new Promise((resolve, reject) => {
            context.commit(AUTH_INIT);
            // Check if session is expired then return to login
            let payload = {
                influencer_id: 8
            };
            console.log("check session: ", payload);
            riaServiceEnpoint
                .influencerPost(payload)
                .then(response => {
                    console.log(response);
                    resolve(response);
                })
                .catch(err => {
                    context.commit(AUTH_ERROR, err);
                    reject(err);
                });
        });
    },

    [AUTH_REQUEST]: (context, auth) => {
        return new Promise((resolve, reject) => {
            console.log("auth code: ", auth.authCode);
            localStorage.setItem("authCode", auth.authCode);
            context.commit(AUTH_REQUEST);
            const payload = {
                refresh_token: "",
                auth_code: auth.authCode,
                id_token: "",
                platform: "web",
                last_login: "0",
                redirect_uri: window.location.origin
            };
            riaServiceEnpoint
                .loginPost(payload)
                .then(response => {
                    console.log("RS res: ", response.data);
                    if (response) {
                        console.log(response);
                        const user = {
                            userToken: response.data.payload.tokeninfo.id_token,
                            refreshToken: response.data.payload.tokeninfo.refresh_token,
                            email: response.data.payload.userinfo.email,
                            userImage: response.data.payload.userinfo.picture,
                            fullName: response.data.payload.userinfo.name,
                            lastLogin: response.data.payload.userinfo.iat
                                // userType: response.data.userType,
                                // availableInfluencers: response.data.availableInfluencers,
                        };
                        context.commit(AUTH_SUCCESS, user);
                        resolve(response);
                    }
                })
                .catch(err => {
                    context.commit(AUTH_ERROR, err);
                    localStorage.removeItem("authCode");
                    reject(err);
                });
        });
    },

    [AUTH_LOGOUT]: ({ commit }) => {
        return new Promise(resolve => {
            commit(AUTH_LOGOUT);
            resolve();
        });
    }
};

const mutations = {
    [AUTH_REQUEST]: state => {
        state.status = "loading";
    },
    [AUTH_SUCCESS]: (state, user) => {
        console.log("user:", user);
        let influencerId;
        let influencerType;
        // if (user["availableInfluencers"].length > 0) {
        //   let labels = user["availableInfluencers"].map((account) => account.label);
        //   console.log(labels);

        //   let demoLabel = "Demo - Rising Star";
        //   let myAccountLabel = "My Account";

        //   let isDemoAccount = labels.includes(demoLabel);
        //   let isMyAccount = labels.includes(myAccountLabel);

        //   let demoAccount = user["availableInfluencers"].filter(
        //     (account) => account.label === demoLabel
        //   );
        //   let myAccount = user["availableInfluencers"].filter(
        //     (account) => account.label === myAccountLabel
        //   );

        //   if (isDemoAccount) {
        //     influencerId = demoAccount[0].id;
        //     influencerType = demoAccount[0].label;
        //   } else if (isMyAccount) {
        //     influencerId = myAccount[0].id;
        //     influencerType = myAccount[0].label;
        //   }
        // } else {
        //   influencerId = 0;
        //   influencerType = "";
        // }

        state.status = "success";
        state.token = user["userToken"];
        state.hasLoadedOnce = true;
        // state.userType = user["userType"];
        // state.availableInfluencers = user["availableInfluencers"];
        state.email = user["email"];
        state.fullName = user["fullName"];
        state.profileImageUrl = user["userImage"];
        // state.influencerId = influencerId;
        // state.influencerType = influencerType;

        localStorage.setItem("lastLogin", user["lastLogin"]);
        localStorage.setItem("userToken", user["userToken"]);
        localStorage.setItem("refreshToken", user["refreshToken"]);
        localStorage.setItem("fullName", user["fullName"]);
        localStorage.setItem("email", user["email"]);
        localStorage.setItem("imageUrl", user["userImage"]);
        localStorage.setItem("userType", user["userType"]);
        localStorage.setItem("influencerId", influencerId);
        localStorage.setItem("influencerType", influencerType);
        localStorage.setItem("validInfluencer", true);
        localStorage.setItem(
            "availableInfluencers",
            JSON.stringify(user["availableInfluencers"])
        );
    },
    [AUTH_ERROR]: state => {
        state.status = "error";
        state.hasLoadedOnce = true;
    },
    [AUTH_LOGOUT]: state => {
        state.token = "";
        state.userType = "";
        state.availableInfluencers = [];
        state.email = "";
        state.fullName = "";
        state.profileImageUrl = "";
        state.influencerId = "";
        state.influencerType = "";

        localStorage.removeItem("authCode");
        localStorage.removeItem("userToken");
        localStorage.removeItem("refreshToken");
        localStorage.removeItem("fullName");
        localStorage.removeItem("imageUrl");
        localStorage.removeItem("userType");
        localStorage.removeItem("availableInfluencers");
        localStorage.removeItem("email");
        localStorage.removeItem("influencerId");
        localStorage.removeItem("influencerType");
        localStorage.removeItem("validInfluencer");
        localStorage.removeItem("isEnter");
    },
    [AUTH_INIT]: state => {
        if (localStorage.getItem("influencerId")) {
            state.influencerId = localStorage.getItem("influencerId");
        }
        if (localStorage.getItem("influencerType")) {
            state.influencerType = localStorage.getItem("influencerType");
        }
        if (localStorage.getItem("fullName")) {
            state.fullName = localStorage.getItem("fullName");
        }
        if (localStorage.getItem("email")) {
            state.email = localStorage.getItem("email");
        }
        if (localStorage.getItem("imageUrl")) {
            state.profileImageUrl = localStorage.getItem("imageUrl");
        }
        if (localStorage.getItem("availableInfluencers")) {
            state.availableInfluencers = JSON.parse(
                localStorage.getItem("availableInfluencers")
            );
        }
        if (localStorage.getItem("userType")) {
            state.userType = localStorage.getItem("userType");
        }
    },
    [AUTH_INFLUENCER_SET]: (state, influencer) => {
        state.influencerId = influencer.influencer_id;
        state.influencerType = influencer.label;
    },
    [AUTH_UPDATE_TOKEN]: (state, userToken) => {
        state.token = userToken;
        localStorage.setItem("userToken", userToken);
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
export default {
  GETTER_DATA_SEARCH(state) {
    let data = [...state.DATA_SEARCH];
    return data.reverse();
  },
  GETTER_DATA_TOP_PICK(state) {
    return state.DATA_TOP_PICK;
  },
  GETTER_DATA_PRODUCT_OF_ROW(state) {
    return state.DATA_PRODUCT_OF_ROW;
  },
  GETTER_DATA_HAS_PRODUCT(state) {
    return state.DATA_SEARCH && state.DATA_SEARCH.length > 0;
  },
  GETTER_DATA_BLOGGER_NAME_ENABLE(state) {
    return state.DATA_BLOGGER_NAME_OPTIONS;
  },
  GETTER_DATA_BLOGGER_CTA_ENABLE(state) {
    return state.DATA_BLOGGER_CTA_OPTIONS;
  },
  GETTER_DATA_ADVERTISEMENT_ENABLE(state) {
    return state.DATA_ADVERTISEMENT_OPTIONS;
  },
  GETTER_DATA_IS_LEADER_BOARD_IMAGE(state) {
    return !!state.DATA_LEADERBOARD_IMAGE;
  },
  GETTER_DATA_LEADER_BOARD_IMAGE(state) {
    return state.DATA_LEADERBOARD_IMAGE;
  },
  GETTER_DATA_IS_MOBILE_IMAGE(state) {
    return !!state.DATA_MOBILE_IMAGE;
  },
  GETTER_DATA_MOBILE_IMAGE(state) {
    return state.DATA_MOBILE_IMAGE;
  },
  GETTER_DATA_IS_ADV_LINK(state) {
    return !!state.DATA_ADV_LINK;
  },
  GETTER_DATA_ADV_LINK(state) {
    return state.DATA_ADV_LINK;
  },
  GETTER_TOP_PICK(state) {
    return state.TOP_PICK;
  },
  GETTER_TITLE_TOP_PICK(state) {
    return state.TITLE_TOP_PICK;
  },
  GETTER_DATA_BANNER(state) {
    return state.DATA_BANNER;
  },
  GETTER_THEME_EMAIL(state) {
    return state.THEME_EMAIL;
  }
};

/* eslint-disable no-console */
import actions from "./actions.js";
import mutations from "./mutations.js";
import getters from "./getters.js";
import { product } from "../../utils/product";

const newrow = () => {
  let arr = [];
  for (let index = 0; index < 3; index++) {
    arr.push({
      blogger_name: null,
      hero_image: null,
      blog_url: null,
      id: null,
      share_url: null,
      infor_hero_image: null,
      shopthelook: "SHOP THE LOOK",
      products: product()
    });
  }
  return arr;
};

export default {
  state() {
    return {
      DATA_SEARCH: [],
      TOP_PICK: "TREND ALERT",
      DATA_PRODUCT_OF_ROW: 8,
      DATA_BLOGGER_NAME_OPTIONS: true,
      DATA_BLOGGER_CTA_OPTIONS: true,
      DATA_ADVERTISEMENT_OPTIONS: true,
      DATA_LEADERBOARD_IMAGE: "",
      DATA_MOBILE_IMAGE: "",
      DATA_ADV_LINK: "",
      TITLE_TOP_PICK:
        "Casual chic for spring has never been easier. Check out this week’s looks from some of Instagram’s most stylish bloggers!",
      DATA_TOP_PICK: newrow(),
      DATA_BANNER: [
        { adv_link: null, leaderBoardImage: null, mobile_image: null }
      ],
      THEME_EMAIL: "Responsive Email"
    };
  },
  actions,
  mutations,
  getters
};

import Repository from "@/repositories/RepositoryFactory";

const riaServiceEnpoint = Repository.get("riaServiceEnpoint");

export default {
  // For API v1
  async LTKS_REQUEST({ commit }, value) {
    try {
      const response = await riaServiceEnpoint.ltks(value.params);
      // console.log("response: " + response.data.data.id);
      if (response.data.data.id) {
        let data = response.data.data;
        let products = [];
        data.products.map(e => {
          products.push({
            image_url: e.image_url,
            hyperlink: e.links.ltk_web,
            type: 0
          });
        });
        let hero_image = data.hero_image;
        let blogger_name = data.profile.display_name;
        let share_url = data.share_url;
        let avatar_url = data.profile.avatar_url;
        commit("MUTATIONS_DATA_SEARCH", {
          blogger_name,
          hero_image,
          products,
          id: value.params,
          type: 1,
          share_url,
          avatar_url
        });
        commit("MUTATIONS_INSERT_DATA_TOP_PICK_AFTER_ADD_URL", {
          blogger_name,
          hero_image,
          id: value.params,
          products,
          share_url,
          index: value.index
        });
      }
      return response.data.data.id;
    } catch (error) {
      console.log(error);
    }
  },

  // For API v2
  async RS_REQUEST({ commit }, value) {
    try {
      let CHANGE_SIZE_IMAGE = url => {
        var arr = url.split("&");
        arr[1] = "width=300";
        arr[2] = "height=300";
        return arr.join("&");
      };
      // crop thumbile image
      //   for (const element of data.products) {
      //     let formdata = new FormData();
      //     formdata.append("image_url", element.image_url);
      //     let cropHeroImage = await riaServiceEnpoint.CropImage(formdata);
      //     if (cropHeroImage) {
      //       console.log(jsonData(cropHeroImage).url_image_150_150);
      //       products.push({
      //         image_url: jsonData(cropHeroImage).url_image_150_150,
      //         hyperlink: element.hyperlink,
      //         type: 0
      //       });
      //     }
      //   }

      const response = await riaServiceEnpoint.rewardStyle(value.params);
      // console.log("response: " + response.data.ltks[0].id);

      if (response.data.ltks[0].id) {
        let data = response.data;
        let products = [];
        data.products.map((e, index) => {
          products.push({
            image_url: CHANGE_SIZE_IMAGE(e.image_url),
            hyperlink: e.hyperlink,
            type: 0
          });
        });

        let hero_image = data.ltks[0].hero_image;
        // data json_file hero_image
        let list_crop = {};
        list_crop.hero_image = {
          image_url: hero_image,
          width: 400,
          height: 500,
          crop_status:'True',
        };
        //
        let infor_hero_image = {
          hero_image_height: data.ltks[0].hero_image_height,
          hero_image_width: data.ltks[0].hero_image_width
        };
        let blogger_name = data.profiles[0].display_name;
        let share_url = data.ltks[0].share_url;
        let avatar_url = data.profiles[0].avatar_url;
        let blog_url = 'https://www.liketoknow.it/'+data.profiles[0].display_name;

        commit("MUTATIONS_DATA_SEARCH", {
          blogger_name,
          hero_image,
          products,
          id: value.params,
          type: 1,
          share_url,
          avatar_url,
          blog_url,
          infor_hero_image
        });
        commit("MUTATIONS_INSERT_DATA_TOP_PICK_AFTER_ADD_URL", {
          blogger_name,
          hero_image,
          id: value.params,
          products,
          share_url,
          index: value.index,
          blog_url,
          infor_hero_image
        });
        // call api crop hero_image
        let cropHeroImage = await riaServiceEnpoint.CropImage(list_crop);
        if (cropHeroImage) {
          let hero_image = cropHeroImage.data.hero_image.image_url_out;
          commit("MUTATIONS_UPDATE_TOP_PICK_BY_ID", {
            hero_image,
            id: value.params
          });
          commit("MUTATIONS_UPDATE_DATA_SEARCH_BY_ID", {
            hero_image,
            id: value.params
          });
        }
      }
      return response.data.ltks[0].id;
    } catch (error) {
      console.log(error);
    }
  },

  ACTION_CROP_IMAGE_FROM_USER({ commit }, value) {
    try {
      commit("MUTATIONS_UPDATE_TOP_PICK_BY_ID", value);
    } catch (error) {
      console.log(error);
    }
  },

  ACTION_INSERT_TOP_PICK({ commit }, value) {
    try {
      commit("MUTATIONS_INSERT_DATA_TOP_PICK", value);
    } catch (error) {
      console.log(error);
    }
  },

  ACTION_DELETE_TOP_PICK({ commit }, value) {
    try {
      commit("MUTATIONS_DELETE_DATA_TOP_PICK", value);
    } catch (error) {
      console.log(error);
    }
  },

  ACTION_INSERT_TOP_PICK_SUB_IMAGE({ commit }, value) {
    try {
      commit("MUTATIONS_INSERT_TOP_PICK_SUB_IMAGE", value);
    } catch (error) {
      console.log(error);
    }
  },

  ACTION_CHANGE_SUB_IMAGE({ commit }, value) {
    try {
      commit("MUTATIONS_CHANGE_SUB_IMAGE", value);
    } catch (error) {
      console.log(error);
    }
  },

  ACTION_DELETE_IMAGE_PRODUCT_TOP_PICK({ commit }, value) {
    try {
      commit("MUTATIONS_IMAGE_PRODUCT_TOP_PICK", value);
    } catch (error) {
      console.log(error);
    }
  },

  ACTION_ADD_NEW_ROW({ commit }) {
    commit("MUTATIONS_ADD_NEW_ROW");
  },

  ACTION_CHANGE_NUM_OF_ROW({ commit }, value) {
    commit("MUTATIONS_CHANGE_NUM_OF_ROW", value);
  },

  ACTION_CHOOSE_TOP_PICK_BY_ID({ commit }, value) {
    commit("MUTATIONS_CHOOSE_TOP_PICK_BY_ID", value);
  },

  ACTION_CHANGE_POSITION_TOP_PICK({ commit }, value) {
    commit("MUTATIONS_CHANGE_POSITION_TOP_PICK", value);
  },

  ACTION_CHANGE_NAME_TOP_PICK_BY_ID({ commit }, value) {
    commit("MUTATIONS_CHANGE_NAME_TOP_PICK_BY_ID", value);
  },

  ACTION_CHANGE_TOP_PICK({ commit }, value) {
    commit("MUTATIONS_CHANGE_TOP_PICK", value);
  },

  ACTION_CHANGE_SHOP_THE_LOOK_TOP_PICK_BY_ID({ commit }, value) {
    commit("MUTATIONS_CHANGE_SHOP_THE_LOOK_TOP_PICK_BY_ID", value);
  },

  ACTION_CHANGE_TITLE_TOP_PICK({ commit }, value) {
    commit("MUTATIONS_CHANGE_TITLE_TOP_PICK", value);
  },

  ACTION_AUTO_FILTER_IMAGE_PRODUCT({ commit }, value) {
    commit("MUTATIONS_AUTO_FILTER_IMAGE_PRODUCT", value);
  },

  ACTION_SET_FILTER(context, payload) {
    switch (payload.id) {
      case "product":
        context.commit("MUTATIONS_CHANGE_NUM_OF_ROW", payload.value);
        // context.commit("MUTATIONS_UPDATE_SUB_IMAGES", payload.value);
        break;
      case "cta":
        context.commit("MUTATIONS_CHANGE_BLOGGER_CTA", payload.value);
        break;
      case "name":
        context.commit("MUTATIONS_CHANGE_BLOGGER_NAME", payload.value);
        break;
      case "adv":
        context.commit("MUTATIONS_CHANGE_ADVERTISEMENT", payload.value);
        break;
      default:
        console.log("Nothing");
    }
  },

  ACTION_DELETE_ROW({ commit }) {
    commit("MUTATIONS_DELETE_ROW");
  },

  ACTION_LOAD_TEMPLATE({ commit }, value) {
    commit("MUTATIONS_LOAD_TEMPLATE", value);
  },

  ACTION_ADD_ADV(context, payload) {
    context.commit("MUTATIONS_ADD_ADV", payload);
  },

  ACTION_DELETE_BANNER_BY_ID(context, payload) {
    context.commit("MUTATIONS_DELETE_BANNER_BY_ID", payload);
  },

  ACTION_CHANGE_THEME_EMAIL(context, payload) {
    context.commit("MUTATIONS_CHANGE_THEME_EMAIL", payload);
  },

  ACTION_CROP_HERO_IMAGE_BY_ID(context, payload) {
    context.commit("MUTATIONS_CROP_HERO_IMAGE_BY_ID", payload);
  },

  ACTION_GET_DATA_TOPICK(context, payload) {
    context.commit("MUTATIONS_GET_DATA_TOPICK", payload);
    context.commit("MUTATIONS_GET_DATA_BANNER", payload);
    context.commit("MUTATIONS_CHANGE_TOP_PICK", payload);
    context.commit("MUTATIONS_CHANGE_TITLE_TOP_PICK", payload);
    context.commit("MUTATIONS_CHANGE_THEME_EMAIL", payload.theme);
    context.commit("MUTATIONS_CHANGE_NUM_OF_ROW", payload.productOfRow);
    context.commit("MUTATIONS_CHANGE_BLOGGER_CTA", String(payload.blogger_cta));
    context.commit(
      "MUTATIONS_CHANGE_BLOGGER_NAME",
      String(payload.nameBloggerEnable)
    );
  }
};

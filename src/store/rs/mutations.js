import { product, newrow, newbanner } from "../../utils/product";
import _ from "lodash";
export default {
  // INSERT NEW DATA SEARCH
  MUTATIONS_DATA_SEARCH(state, value) {
    let data = [...state.DATA_SEARCH];
    let productOfRow = state.DATA_PRODUCT_OF_ROW;
    let numOfProducts = null;
    let indexDataSearchChoose = data.findIndex((e) => e.id == value.id);
    if (indexDataSearchChoose >= 0) {
      data.forEach((element) => {
        element.type = 0;
      });
      data[indexDataSearchChoose].type = 1;
      // console.log(data.products);
      if (data.products) {
        numOfProducts =
          data.products.length >= productOfRow
            ? productOfRow
            : data.products.length;
        for (let index = 0; index < productOfRow; index++) {
          data[indexDataSearchChoose].products[index].type = 1;
        }
      }
    } else {
      // console.log(" vao day", value);
      data.forEach((element) => {
        element.type = 0;
      });
      // check độ dài product của url nhập và product của row
      numOfProducts =
        value.products.length >= productOfRow
          ? productOfRow
          : value.products.length;
      // console.log(numOfProducts);
      for (let index = 0; index < numOfProducts; index++) {
        value.products[index].type = 1;
      }

      data.push(value);
    }
    state.DATA_SEARCH = data;
  },
  // INSERT NEW DATA TOP PICK AFTER ADD URL
  MUTATIONS_INSERT_DATA_TOP_PICK_AFTER_ADD_URL(state, value) {
    let data = [...state.DATA_TOP_PICK];
    let productOfRow = state.DATA_PRODUCT_OF_ROW;
    let newProduct = product();

    if (value.index >= 0) {
      data[value.index].blogger_name = value.blogger_name;
      data[value.index].hero_image = value.hero_image;
      data[value.index].id = value.id;
      data[value.index].share_url = value.share_url;
      data[value.index].blog_url = value.blog_url;
      data[value.index].infor_hero_image = value.infor_hero_image;
      // check độ dài product của url nhập và product của row
      let numOfProducts =
        value.products.length >= productOfRow
          ? productOfRow
          : value.products.length;
      for (let index = 0; index < numOfProducts; index++) {
        newProduct[index].image_url = value.products[index].image_url;
        newProduct[index].hyperlink = value.products[index].hyperlink;
      }
      data[value.index].products = newProduct;
    } else {
      let checkDataTopPick = data.filter((e) => e.id == value.id);
      if (checkDataTopPick.length == 0) {
        let dataTopPickCheck = data.filter((e) => e.id == null);
        if (dataTopPickCheck.length > 0) {
          dataTopPickCheck[0].blogger_name = value.blogger_name;
          dataTopPickCheck[0].hero_image = value.hero_image;
          dataTopPickCheck[0].id = value.id;
          dataTopPickCheck[0].share_url = value.share_url;
          dataTopPickCheck[0].blog_url = value.blog_url;
          // check độ dài product của url nhập và product của row
          let numOfProducts =
            value.products.length >= productOfRow
              ? productOfRow
              : value.products.length;
          for (let index = 0; index < numOfProducts; index++) {
            newProduct[index].image_url = value.products[index].image_url;
            newProduct[index].hyperlink = value.products[index].hyperlink;
          }

          dataTopPickCheck[0].products = newProduct;
        }
      }
    }

    state.DATA_TOP_PICK = data;
  },
  // INSERT NEW DATA TOP PICK FOLLOW ROW INDEX
  MUTATIONS_INSERT_DATA_TOP_PICK(state, value) {
    let productOfRow = state.DATA_PRODUCT_OF_ROW;
    let data = [...state.DATA_TOP_PICK];
    let dataSearch = [...state.DATA_SEARCH];
    let dataTopPickById = data[value.index];
    dataTopPickById.blogger_name = value.blogger_name;
    dataTopPickById.hero_image = value.hero_image;
    dataTopPickById.id = value.id;
    dataTopPickById.share_url = value.share_url;
    dataTopPickById.blog_url = value.blog_url;
    dataTopPickById.infor_hero_image = value.infor_hero_image;
    dataTopPickById.products = product();
    // check độ dài product của url nhập và product của row
    let numOfProducts =
      value.products.length >= productOfRow
        ? productOfRow
        : value.products.length;
    for (let index = 0; index < numOfProducts; index++) {
      dataTopPickById.products[index].image_url =
        value.products[index].image_url;
      dataTopPickById.products[index].hyperlink =
        value.products[index].hyperlink;
    }

    let dataSearchById = dataSearch.filter((e) => e.id == value.id);
    dataSearchById[0].products.forEach((element, index) => {
      if (index < productOfRow) {
        element.type = 1;
      }
    });
    state.DATA_TOP_PICK = data;
  },
  // DELETE DATA TOP PICK BY ID
  MUTATIONS_DELETE_DATA_TOP_PICK(state, value) {
    // set data top pick after delete
    let data = [...state.DATA_TOP_PICK];
    data[value.index].blogger_name = null;
    data[value.index].id = null;
    data[value.index].hero_image = null;
    data[value.index].name = null;
    data[value.index].share_url = null;
    data[value.index].blog_url = null;
    data[value.index].infor_hero_image = null;
    data[value.index].products = product();
    state.DATA_TOP_PICK = data;
    // set data search after delete
    let dataSearch = [...state.DATA_SEARCH];
    let indexSeacrch = dataSearch.findIndex((e) => e.id == value.id);
    dataSearch[indexSeacrch].products.forEach((element) => {
      element.type = 0;
    });
    state.DATA_SEARCH = dataSearch;
  },
  // INSERT SUB IMAGE BY TOP PICK BY ID
  MUTATIONS_INSERT_TOP_PICK_SUB_IMAGE(state, value) {
    let productOfRow = state.DATA_PRODUCT_OF_ROW;
    let data = [...state.DATA_TOP_PICK];
    let dataById = data[value.index];
    let dataTopPickCheck = dataById.products.filter(
      (e) => e.image_url == value.image_url
    );
    if (dataTopPickCheck.length > 0) {
      dataTopPickCheck[0].image_url = "";
    } else {
      if (
        dataById.products.filter((e) => e.image_url != "").length < productOfRow
      ) {
        let dataProductNotTopPick = dataById.products.filter(
          (e) => e.image_url == ""
        );
        if (dataProductNotTopPick.length > 0) {
          dataProductNotTopPick[0].image_url = value.image_url;
        }
      }
    }
    let arrayChoose = dataById.products.filter((e) => e.image_url.length > 0);
    let arrayNotChoose = dataById.products.filter((e) => e.image_url == "");
    dataById.products = arrayChoose.concat(arrayNotChoose);

    state.DATA_TOP_PICK = data;
  },
  // CHANGE TYPE SHOW DATA SEARCH
  MUTATIONS_CHANGE_SUB_IMAGE(state, value) {
    let productOfRow = state.DATA_PRODUCT_OF_ROW;
    let data = [...state.DATA_SEARCH];
    let index = data.findIndex((e) => e.id == value.id);
    let check = data[index].products.findIndex(
      (e) => e.image_url == value.image_url
    );
    if (check >= 0) {
      if (data[index].products[check].type == 1) {
        data[index].products[check].type = 0;
      } else {
        if (
          data[index].products.filter((e) => e.type == 1).length < productOfRow
        ) {
          data[index].products[check].type = 1;
        }
      }
    }
    state.DATA_SEARCH = data;
  },
  //CHANGE NAME TOPICK BY ID
  MUTATIONS_CHANGE_NAME_TOP_PICK_BY_ID(state, value) {
    let data = [...state.DATA_TOP_PICK];
    let index = data.findIndex((e) => e.id == value.id);
    data[index].blogger_name = value.blogger_name;
    state.DATA_TOP_PICK = data;
  },
  // CHANG NAME SHOP THE LOOK
  MUTATIONS_CHANGE_SHOP_THE_LOOK_TOP_PICK_BY_ID(state, value) {
    let data = [...state.DATA_TOP_PICK];
    let index = data.findIndex((e) => e.id == value.id);
    data[index].shopthelook = value.shopthelook;
    state.DATA_TOP_PICK = data;
  },

  // CHANGE SUB IMAGE FROM BLOG LIST AND TOP PICK
  MUTATIONS_IMAGE_PRODUCT_TOP_PICK(state, value) {
    let data = [...state.DATA_TOP_PICK];
    let dataById = data[value.index];
    let indexProduct = dataById.products.findIndex(
      (e) => e.image_url == value.image_url
    );
    dataById.products[indexProduct].image_url = "";
    let arrayChoose = dataById.products.filter((e) => e.image_url.length > 0);
    let arrayNotChoose = dataById.products.filter((e) => e.image_url == "");
    dataById.products = arrayChoose.concat(arrayNotChoose);
    state.DATA_TOP_PICK = data;
    // set data search after delete
    let dataSearch = [...state.DATA_SEARCH];
    let dataSearchDetail = dataSearch.filter((e) => e.id == dataById.id)[0];
    let dataSearchByIndex = dataSearchDetail.products.filter(
      (e) => e.image_url == value.image_url
    );
    dataSearchByIndex[0].type = 0;
    state.DATA_SEARCH = dataSearch;
  },
  // ADD NEW ROW TOP PICK
  MUTATIONS_ADD_NEW_ROW(state) {
    let DATA_TOP_PICK = [...state.DATA_TOP_PICK, ...newrow()];
    let DATA_BANNER = [...state.DATA_BANNER, ...newbanner()];
    state.DATA_TOP_PICK = DATA_TOP_PICK;
    state.DATA_BANNER = DATA_BANNER;
  },

  MUTATIONS_CHANGE_TOP_PICK(state, value) {
    state.TOP_PICK = value.topPick;
  },

  MUTATIONS_CHANGE_NUM_OF_ROW(state, value) {
    console.log(value);
    state.DATA_PRODUCT_OF_ROW = value;
  },

  MUTATIONS_CHOOSE_TOP_PICK_BY_ID(state, value) {
    let data = [...state.DATA_SEARCH];
    data.forEach((element) => {
      if (element.id == value.id) {
        element.type = 1;
      } else {
        element.type = 0;
      }
    });
  },

  MUTATIONS_CHANGE_POSITION_TOP_PICK(state, value) {
    let data = [...state.DATA_TOP_PICK];
    let dataFrom = data[value.from];
    let dataTo = data[value.to];
    data[value.to] = dataFrom;
    data[value.from] = dataTo;
    state.DATA_TOP_PICK = data;
  },

  MUTATIONS_CHANGE_TITLE_TOP_PICK(state, value) {
    state.TITLE_TOP_PICK = value.titleTopPick;
  },

  MUTATIONS_CHANGE_BLOGGER_NAME(state, payload) {
    const status = payload === "true" ? true : false;
    state.DATA_BLOGGER_NAME_OPTIONS = status;
  },

  MUTATIONS_CHANGE_BLOGGER_CTA(state, payload) {
    const status = payload === "true" ? true : false;
    state.DATA_BLOGGER_CTA_OPTIONS = status;
  },

  MUTATIONS_CHANGE_ADVERTISEMENT(state, payload) {
    const status = payload === "true" ? true : false;
    state.DATA_ADVERTISEMENT_OPTIONS = status;
  },

  MUTATIONS_UPDATE_SUB_IMAGES(state, payload) {
    const numOfProducts = parseInt(payload.value);
  },

  MUTATIONS_LOAD_TEMPLATE(state, value) {
    console.log(value);
    state.DATA_TOP_PICK = value.DATA_TOP_PICK;
    state.DATA_BANNER = value.DATA_BANNER;
    state.DATA_SEARCH = value.DATA_SEARCH;
    state.DATA_BLOGGER_NAME_OPTIONS = value.DATA_BLOGGER_NAME_OPTIONS;
    state.DATA_BLOGGER_CTA_OPTIONS = value.DATA_BLOGGER_CTA_OPTIONS;
    state.DATA_PRODUCT_OF_ROW = parseFloat(value.DATA_PRODUCT_OF_ROW);
  },

  MUTATIONS_DELETE_ROW(state) {
    let DATA_TOP_PICK = [...state.DATA_TOP_PICK];
    let DATA_BANNER = [...state.DATA_BANNER];
    if (DATA_TOP_PICK.length > 3) {
      state.DATA_TOP_PICK = DATA_TOP_PICK.slice(0, DATA_TOP_PICK.length - 3);
      state.DATA_BANNER = DATA_BANNER.slice(0, (DATA_TOP_PICK.length - 3) / 3);
    }
  },

  MUTATIONS_DELETE_BANNER_BY_ID(state, payload) {
    let DATA_BANNER = [...state.DATA_BANNER];
    let dataBannerById = DATA_BANNER[payload.index];
    dataBannerById.adv_link = null;
    dataBannerById.mobile_image = null;
    dataBannerById.leaderBoardImage = null;
    state.DATA_BANNER = DATA_BANNER;
  },

  MUTATIONS_ADD_ADV(state, payload) {
    console.log("MUTATIONS_ADD_ADV ->>", payload);
    let DATA_BANNER = [...state.DATA_BANNER];
    let index = payload.idx;
    DATA_BANNER[index].adv_link = payload.advLink;
    DATA_BANNER[index].mobile_image = payload.mobileImage;
    DATA_BANNER[index].leaderBoardImage = payload.leaderBoardImage;
    state.DATA_BANNER = DATA_BANNER;
  },

  MUTATIONS_CHANGE_THEME_EMAIL(state, payload) {
    state.THEME_EMAIL = payload;
  },

  MUTATIONS_CROP_HERO_IMAGE_BY_ID(state, payload) {
    let DATA_TOP_PICK = [...state.DATA_TOP_PICK];
    let dataTopPickById = DATA_TOP_PICK.filter((e) => e.id == payload.id)[0];
    dataTopPickById.hero_image = payload.hero_image;
    state.DATA_TOP_PICK = DATA_TOP_PICK;
  },

  MUTATIONS_GET_DATA_TOPICK(state, payload) {
    state.DATA_TOP_PICK = payload.dataTopPick;
  },

  MUTATIONS_GET_DATA_BANNER(state, payload) {
    state.DATA_BANNER = payload.banner;
  },

  MUTATIONS_UPDATE_TOP_PICK_BY_ID(state, value) {
    let data = [...state.DATA_TOP_PICK];
    let index = data.findIndex(e => e.id == value.id);
    data[index].hero_image = value.hero_image;
    state.DATA_TOP_PICK = data;
  },

  MUTATIONS_UPDATE_DATA_SEARCH_BY_ID(state, value) {
    let data = [...state.DATA_SEARCH];
    let index = data.findIndex(e => e.id == value.id);
    data[index].hero_image = value.hero_image;
    state.DATA_SEARCH = data;
  }
};

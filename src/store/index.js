import Vue from "vue";
import Vuex from "vuex";
import rsx from "./rs/index";
import auth from "./modules/auth";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    rsx
  }
});


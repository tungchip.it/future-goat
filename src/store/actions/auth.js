export const AUTH_REQUEST = "AUTH_REQUEST";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_ERROR = "AUTH_ERROR";
export const AUTH_LOGOUT = "AUTH_LOGOUT";
export const AUTH_INIT = "AUTH_INIT";
export const AUTH_INFLUENCER_SET = "AUTH_INFLUENCER_SET";
export const AUTH_UPDATE_TOKEN = "AUTH_UPDATE_TOKEN";

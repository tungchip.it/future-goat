import Vue from "vue";
import Iframe from "./Iframe.vue";
import router from "./router/iframe";
import store from "@/store/index";

new Vue({
  router,
  store,
  render: (h) => h(Iframe),
}).$mount("#iframe");

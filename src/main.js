import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/store/index";
import GoogleAuth from "./config/google_oAuth.js";
import BaseSpinner from "./components/common/BaseSpinner.vue";
import BaseButton from "./components/common/BaseButton.vue";
import Notify from "./components/Notify.vue";
import VueRx from 'vue-rx'
import VuejsClipper from 'vuejs-clipper'

Vue.use(VuejsClipper)
// install vue-rx
Vue.use(VueRx)
Vue.component('base-spinner', BaseSpinner);
Vue.component('base-button', BaseButton);
Vue.component('Notify', Notify);

const gauthOption = {
  clientId:
    "179577982038-ek1vq4dc6i2kkg9siltk70f50o5rhed0.apps.googleusercontent.com",
  scope: "email profile openid",
  prompt: "consent",
  response_type: "id_token permission code",
  hosted_domain: "rewardstyle.com",
};

Vue.use(GoogleAuth, gauthOption);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

import Vue from "vue";
import VueRouter from "vue-router";
import Iframe from "../views/Iframe.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/iframe",
    name: "Iframe",
    component: Iframe
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;

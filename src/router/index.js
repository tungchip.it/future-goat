import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Iframe from "../views/Iframe.vue";
import HtmlView from "../views/HtmlView.vue";
import Login from "@/components/Login";

import store from "@/store/index";

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return;
    }
    next("/");
};

const ifAuthenticated = (to, from, next) => {
    const isAuth = store.getters.isAuthenticated;
    console.log("isAuthenticated: ", isAuth)
    if (isAuth) {
        next();
        return;
    }
    next("/login");
};

const routes = [{
        path: "/",
        name: "Home",
        component: Home,
        // beforeEnter: ifAuthenticated
    },
    {
        path: "/iframe",
        name: "Iframe",
        component: Iframe,
        // beforeEnter: ifAuthenticated
    },
    {
        path: "/htmlview",
        name: "HtmlView",
        component: HtmlView,
        // beforeEnter: ifAuthenticated
    },
    {
        path: "/login",
        name: "login",
        component: Login
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

export default router;
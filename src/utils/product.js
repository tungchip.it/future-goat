const product = () => {
    let arr = [];
    for (let index = 0; index < 30; index++) {
        arr.push({ id: index + 1, image_url: "" });
    }
    return arr;
};

const newrow = () => {
    let arr = [];
    for (let index = 0; index < 3; index++) {
        arr.push({
            blogger_name: null,
            hero_image: null,
            blog_url: null,
            id: null,
            share_url: null,
            shopthelook: "SHOP THE LOOK",
            products: product()
        });
    }
    return arr;
};

const newbanner = () => {
    return [{
        adv_link: null,
        mobile_image: null,
        leaderBoardImage: null
    }];
};

export { product, newrow, newbanner };
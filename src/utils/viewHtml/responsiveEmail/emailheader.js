import store from "../../../store/index";
const emailHeader = () => {
  let TOP_PICK = store.getters.GETTER_TOP_PICK;
  let TITLE_TOP_PICK = store.getters.GETTER_TITLE_TOP_PICK;
  return `
  <table role="presentation" valign="top" width="100%" cellspacing="0" cellpadding="20" border="0" align="center" clicktracking=off>
    <tbody width="100%" clicktracking=off>
      <tr width="100%" clicktracking=off>
        <td style="width: 40%; padding:10px 0px 10px 15px;">
          <a clicktracking="off" href="https://www.liketoknow.it/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="text-decoration: none;" target="_blank"> <img src="https://rs-folder.s3.amazonaws.com/img/LTK-logo-red.png" style="width: 100%; height: auto; border: 0; box-shadow: none; max-width: 200px; display: block;" /></a>
        </td>
        <td style="padding: 10px 15px 10px 0;" width="60%" align="right">
          <a clicktracking="off" class="nav-link" style="text-decoration: none; font-weight: 600; font-size: medium; color: #e34e4d; padding: 10px 0;" href="https://about.liketoknow.it/about?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" target="_blank">SHOP</a>
          <a clicktracking="off" class="nav-link" style="text-decoration: none; font-size: medium; padding: 10px 0; padding-left: 5%; font-weight: 600; color: #e34e4d;" href="https://www.liketoknow.it/liked/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" target="_blank">MY LIKES</a>
        </td>
      </tr>
    </tbody>
  </table>

  <table role="presentation" valign="top" style="width: 100%; border-top: 4px solid #e34e4d;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
    <tbody>
      <tr>
        <td width="33.3333%"></td>
        <td style="background-color: #e34e4d;text-align: center;" width="33.3333%">
          <h1 style="color: white; font-size: 2.8vw; font-family: Gotham, 'Gill Sans', sans-serif; font-weight: normal; letter-spacing: 1px; padding: 15px 10px; margin: 0 auto;">${TOP_PICK}</h1>
        </td>
        <td width="33.3333%"></td>
      </tr>
    </tbody>
  </table>


  <table role="presentation" valign="top" width="100%" cellspacing="0" cellpadding="20" align="center" style="margin: 0 auto 2vw; border-bottom: 1px solid #e4e4e4; background-color: #fff; width: 100%;">
    <tbody>
      <tr>
        <td style="padding: 1vw 15px 3vw;">
          <h2 class="head-text" style="color: #888888 !important; font-size:medium;display: block; font-family: Didot, 'Palatino Linotype', Palatino, 'Book Antigua', serif; font-style: italic; font-weight: normal; line-height: 130%; margin: 0; text-align: center;">
          ${TITLE_TOP_PICK}
          </h2>
        </td>
      </tr>
    </tbody>
  </table>


`;
};

export { emailHeader };

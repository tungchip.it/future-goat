import store from "../../../store/index";
import { BloggerList } from "./components/BloggerList";
import { sale_banner } from "./components/sale_banner";

import _ from "lodash";
let emailMain = () => {
  let dataBanner = store.getters.GETTER_DATA_BANNER;
  let dataTopPick = _.chunk(store.getters.GETTER_DATA_TOP_PICK, 3);
  let data = "";
  dataTopPick.forEach((element, index) => {
      data =
        data +
        `<div class="three-col" style="text-align: center; font-size: 0; border-bottom: 1px solid #e4e4e4;margin: 2vw auto 0">
            <!--[if mso]> <table role="presentation" width="100%"> <tr> <td style="width:33%;padding:10px;" valign="bottom"><![endif]-->
                ${BloggerList(element, index)}
            <!--[if mso]> </td><td style="width:33%;padding:10px;" valign="bottom"><![endif]-->
          </div>
          ${sale_banner(dataBanner[index])}
        `;
  });
  return data;
};

export { emailMain };

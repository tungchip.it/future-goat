const head = () => {
    return `<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="x-apple-disable-message-reformatting" />
    <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no" />
    <style>
      html,
      body {
        margin: 0 auto !important;
        padding: 0;
        height: 100% !important;
        width: 100% !important;
      }
  
      ::-webkit-scrollbar {
        width: 5px;
        height: 5px;
      }
  
      ::-webkit-scrollbar-track {
        background: #f1f1f1;
      }
  
      ::-webkit-scrollbar-thumb {
        background: rgb(187, 186, 186);
      }
  
      /* What it does: Stops email clients resizing small text. */
      * {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }
  
      /* What it does: Centers email on Android 4.4 */
      div[style*="margin: 16px 0"] {
        margin: 0 !important;
      }
  
      /* What it does: Stops Outlook from adding extra spacing to tables. */
      table,
      td {
        mso-table-lspace: 0pt !important;
        mso-table-rspace: 0pt !important;
      }
  
      /* What it does: Fixes webkit padding issue. */
      h2.head-text {
        font-size: 2vw;
      }
  
      table {
        border: 0;
        border-spacing: 0;
        border-collapse: collapse;
      }
  
      /* What it does: Forces Samsung Android mail clients to use the entire viewport. */
      table[class="mw1200"] {
        max-width: 1200px;
      }
  
      table[class="footer"] {
        font-size: 1.5vw;
      }
  
      tr[class="row-bl"] {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
      }
  
      td[class="col-bl"] {
        max-width: calc(33.3333% - 20px);
        -webkit-box-flex: 0;
        -ms-flex: 0 0 calc(33.3333% - 20px);
        flex: 0 0 calc(33.3333% - 20px);
      }
  
      h3[class="name-bloger"] {
        font-size: 18px;
      }
  
      #MessageViewBody,
      #MessageWebViewDiv {
        width: 100% !important;
      }
  
      .btnStore img {
        width: 180px;
        margin: 0 5px;
        height: 60px;
        border: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
        max-height: 60px;
      }
  
      /* What it does: A work-around for email clients automatically linking certain text strings. */
      /* iOS */
      a[class="btn-cta"] {
        font-size: 2vw;
      }
  
      a[x-apple-data-detectors],
      .unstyle-auto-detected-links a,
      .aBn {
        border-bottom: 0 !important;
        cursor: default !important;
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
      }
  
      .nav-link {
        font-size: 2vw;
      }
    </style>
    <!--[if gte mso 9]>
      <xml>
      <o:OfficeDocumentSettings> <o:AllowPNG /> <o:PixelsPerInch>96</o:PixelsPerInch> </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
  </head>`;
};

export { head };
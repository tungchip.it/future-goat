import { head } from "./head";
import { emailHeader } from "./emailheader";
import { emailMain } from "./emailmain";
import { emailFooter } from "./emailfooter";

const htmlView = () => {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
${head()}
<body width="100%" style="margin: 0; padding: 0; word-spacing: normal;" valign="top" align="center" clicktracking=off>
${emailHeader()}
${emailMain()}
${emailFooter()}
</body>
</html>`;
};
export { htmlView };

let CTA = top_pick => {
    return `
    <div style="width: 100%;">
        <a clicktracking="off" href="${top_pick.share_url? top_pick.share_url+"?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert": "#"}" target="_blank" class="btn-cta" style="display: block;font-size: 18px;font-family: Didot, 'Palatino Linotype', Palatino, serif;text-align: center;border: 1px solid #000000;background-color: #ffffff;color: #000000;font-style: italic;padding: 10px 0;margin: 15px 5px 20px;text-decoration: none;">
            <span>${top_pick.shopthelook}</span>
        </a>
    </div>`;
};

export { CTA };
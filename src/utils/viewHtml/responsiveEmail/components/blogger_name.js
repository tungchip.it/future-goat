const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-/]))?/;

const blogger_name = top_pick => {
  let checkUrl = top_pick => {
    let href = "";
    if (!regexp.test(top_pick.blog_url)) {
      href =
        top_pick.blog_url != null
          ? `href="http://${top_pick.blog_url}?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert"`
          : "";
    } else {
      href = `href="${top_pick.blog_url}?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert"`;
    }
    return href;
  };

  return `
<table valign="top" cellspacing="0" cellpadding="5" border="0" width="100%" align="center">
  <tbody>
    <tr>
      <td colspan="3" valign="middle" align="center">
        <a clicktracking="off" style="text-decoration: none;text-align: center;color: black;display: block;margin: 10px 0 5px;" ${checkUrl(top_pick)} target="_blank">
          <h3 class="name-bloger" style="font-size: large;width:100%; letter-spacing: 1px; text-transform: uppercase; font-weight: normal; font-family: Gotham, 'Gill Sans', sans-serif; text-align: center; overflow: hidden;display: block; margin:0;word-break: break-all;word-wrap: break-word;">${top_pick.blogger_name}</h3>
        </a>
      </td>
    </tr>
  </tbody>
</table>`;
};

export { blogger_name };

let hero_image = top_pick => {
    return `
    <div class="thumb">
        <a clicktracking="off" class="gram_link" style="padding: 5px; display: block;"  href="${top_pick.share_url? top_pick.share_url +"?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert": '#'}" target="_blank">
            <img src="${top_pick.hero_image}" style="width: 100%;" class="hero_img" width="100%" />
        </a>
    </div>`;
};
export { hero_image };
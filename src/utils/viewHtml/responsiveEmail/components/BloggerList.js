import { Blogger } from "./Blogger";
import { HideBloger } from "./HideBloger";

const BloggerList = (dataTopPick, index) => {
  let data = "";
  dataTopPick.forEach(element => {
    if (element.id) {
      data += `${Blogger(element)}`;
    }
  });
  return `${data}`;
};

export { BloggerList };

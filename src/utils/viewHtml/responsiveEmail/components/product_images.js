const product_images = (data, GETTER_DATA_PRODUCT_OF_ROW) => {
        let arr = ``;
        let count = 0;
        let percent;
        switch (parseFloat(GETTER_DATA_PRODUCT_OF_ROW)) {
            case 8:
                percent = "25%";
                break;
            case 6:
                percent = "33.3333%";
                break;
            case 4:
                percent = "25%";
                break;
            case 3:
                percent = "33.3333%";
                break;
            default:
                break;
        }
        data.forEach((element, index) => {
                    arr += `${
      element.image_url && element.image_url
        ? `<td style="float:left;width: calc( ${percent} - 10px); margin: 5px; padding:0">
              <a clicktracking="off" style="display: block;" href="${element.hyperlink}?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" target="_blank">
                <img class="product_img" style="width: 100%; vertical-align: middle;" src="${element.image_url}" width="100%" />
              </a>
            </td>`: 
            `<td style="float:left;width: calc( ${percent}  - 10px); margin: 5px; padding:0">
                <img class="product_img" style="width: 100%; vertical-align: middle;" src="https://ria-ai-hashtag-s3.s3.amazonaws.com/1611200012875_id_1_300_300.jpg" width="100%" />
            </td>
`}`;
    count++;
  });
  return arr;
};
export { product_images };
import store from "../../../store/index";
import { BloggerList } from "./components/BloggerList";
import { sale_banner } from "./components/sale_banner";

import _ from "lodash";
let emailMain = () => {
    let dataBanner = store.getters.GETTER_DATA_BANNER;
    let dataTopPick = _.chunk(store.getters.GETTER_DATA_TOP_PICK, 3);
    let data = "";
    dataTopPick.forEach((element, index) => {
        if (element.filter(e => e.id == null).length < 3) {
            data = data + `
          <table role="presentation" valign="top" cellspacing="0" cellpadding="0" border="0" align="center " style="border-bottom: 1px solid #e4e4e4;margin: 2vw auto 0;background-color: #fff;width: 100%;max-width: 1024px;" class="blogger_table">
            <tbody style="width: 100%">
              <tr style="width: 100%">
              ${BloggerList(element, index)}
              </tr>
            </tbody>
          </table>
          ${sale_banner(dataBanner[index])}
        `;
        }
    });
    return data;
};

export { emailMain };
const head = () => {
    return `<head>
    <title></title>
    <!-- The title tag shows in email notifications, like Android 4.4. -->
    <meta charset="utf-8" />
    <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width" />
    <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting" />
    <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no" />
    <!-- Tell iOS not to automatically link certain text strings. -->
  
    <!-- CSS Reset : BEGIN -->
    <style>
      /* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
  
      html,
      body {
        margin: 0 auto !important;
        padding: 0 !important;
        height: 100% !important;
        width: 100% !important;
      }
  
      /* What it does: Stops email clients resizing small text. */
  
      * {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }
  
      ::-webkit-scrollbar {
        width: 5px;
        height: 5px;
      }
  
      ::-webkit-scrollbar-track {
        background: #f1f1f1;
      }
  
      ::-webkit-scrollbar-thumb {
        background: rgb(187, 186, 186);
      }
  
      /* What it does: Centers email on Android 4.4 */
  
      div[style*="margin: 16px 0"] {
        margin: 0 !important;
      }
  
      /* What it does: Stops Outlook from adding extra spacing to tables. */
      img {
        width: 100%;
        vertical-align: middle;
      }
  
      table,
      td {
        mso-table-lspace: 0pt !important;
        mso-table-rspace: 0pt !important;
      }
  
      /* What it does: Fixes webkit padding issue. */
  
      table {
        border: 0;
        border-spacing: 0;
        border-collapse: collapse;
      }
  
      /* What it does: Forces Samsung Android mail clients to use the entire viewport. */
  
      #MessageViewBody,
      #MessageWebViewDiv {
        width: 100% !important;
      }
  
      /* What it does: Uses a better rendering method when resizing images in IE. */
  
      img {
        -ms-interpolation-mode: bicubic;
      }
  
      /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
  
      a {
        color: black;
        text-decoration: none;
      }
  
      /* What it does: A work-around for email clients automatically linking certain text strings. */
      /* iOS */
  
      a[x-apple-data-detectors],
      .unstyle-auto-detected-links a,
      .aBn {
        border-bottom: 0 !important;
        cursor: default !important;
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
      }
  
      u+#body a,
      /* Gmail */
  
      #MessageViewBody a
  
      /* Samsung Mail */
        {
        color: inherit;
        text-decoration: none;
        font-size: inherit;
        font-family: inherit;
        font-weight: inherit;
        line-height: inherit;
      }
  
      /* What it does: Prevents Gmail from changing the text color in conversation threads. */
  
      .im {
        color: inherit !important;
      }
  
      /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
  
      .a6S {
        display: none !important;
        opacity: 0.01 !important;
      }
  
      /* If the above doesn't work, add a .g-img class to any image in question. */
  
      img.g-img+div {
        display: none !important;
      }
  
      /* What it does: Removes right gutter in Gmail iOS app.  */
      /* Create one of these media queries for each additional viewport size you'd like to fix */
      /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
  
      @media (max-width: 640px) {
        .blogger_cta {
          padding: 6px 0 !important;
        }
      }
    </style>
  </head>`;
};

export { head };
const emailFooter = () => {
  let str_unsub = '{{${preference_center_url}}}';
    return `
  <table role="presentation" valign="top" align="center" style="background: #f2f2f2; background-color: #f2f2f2; width: 100%; font-size: small;" cellspacing="10" cellpadding="5" border="0" class="footer">
    <tbody>
      <tr>
        <td align="center">
          <a clicktracking="off" href="https://www.liketoknow.it/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" target="_blank" style="display: inline-block; color: black; text-transform: capitalize; padding: 5vh 0 1vh;">
            <img src="https://rs-folder.s3.amazonaws.com/img/LTK-logo-red.png" href="https://about.liketoknow.it/" style="height:auto;border: 0;box-shadow: none; width: 45%;" class="footer_ltk_logo">
          </a>
          <br />
          <br />
        </td>
      </tr>
      <tr>
        <td align="center">
          Learn more about
          <a clicktracking="off" target="_blank" href="https://www.rewardstyle.com/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="display: inline-block; color: black; text-transform: capitalize;">influencers</a>
          +
          <a clicktracking="off" target="_blank" href="https://www.rewardstyle.com/about/brands//?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="display: inline-block; color: black; text-transform: capitalize;">brands</a>
        </td>
      </tr>
      <tr>
        <td align="center">
          <a clicktracking="off" target="_blank" href="https://www.liketoknow.it/legal/terms/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="display: inline-block; color: black; text-transform: none;">Terms of Service</a>
          | <a clicktracking="off" target="_blank" href="https://www.liketoknow.it/legal/privacy/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="display: inline-block; color: black; text-transform: capitalize;">privacy policy</a>
          | <a clicktracking="off" target="_blank" href="https://www.liketoknow.it/legal/cookie-policy/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="display: inline-block; color: black; text-transform: capitalize;">cookie policy</a>
          | <a clicktracking="off" target="_blank" href="https://www.liketoknow.it/legal/virtual-patent/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="display: inline-block; color: black; text-transform: capitalize;">patents</a>
          | <a clicktracking="off" target="_blank" href="https://help.liketoknow.it/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="display: inline-block; color: black; text-transform: capitalize;">help center</a>
          | <a clicktracking="off" href="${str_unsub}" style="display: inline-block; color: black; text-transform: capitalize;">unsubscribe</a>
          <br />
          <br />
        </td>
      </tr>
      <tr>
        <td align="center">
          You're receiving this email because you registered for LIKEtoKNOW.it and opted in to our newsletters.
        </td>
      </tr>
      <tr>
        <td align="center">
          rewardStyle 3102 Oak Lawn Ave. 9th Floor Dallas, TX 75219 USA ©
          <a clicktracking="off" target="_blank" href="https://www.liketoknow.it/?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert" style="display: inline-block; color: black; text-transform: capitalize;">LIKEtoKNOW.it</a>
          2021 | All Rights Reserved
          <br />
          <br />
        </td>
      </tr>
      <tr>
        <td align="center">
          This is an auto-generated message from rewardStyle. Please do not reply to this message, as we cannot respond to messages sent to this address.
        </td>
      </tr>
      <tr>
        <td align="center">
          This message and any attachments contain Confidential and Proprietary Information of rewardStyle, Inc. It may be read and used only by the intended recipient. Any use by a person other than its intended recipient, or by the intended
          recipient for purposes other than the intended purpose (included but not limited to reproduction, distribution or taking screenshots of the message or attachments) is strictly prohibited. If you receive this message in error, please notify
          the sender immediately and delete the message and any attachments.
        </td>
      </tr>
      <tr>
        <td align="center" style="padding:20px 0 30px">
          <a clicktracking="off" target="_blank" href="https://ltk.app.link/xBuJxnP6NV" style="display: inline-block; color: black; text-transform: capitalize;">
            <img src="https://static.liketoknow.it/ltk-newsletter-content/2019-01-25/app-store-dl-apple.png" style="width: 118px; height: 36px; margin: 0 5px; border: 0; box-shadow: none;" />
          </a>
          <a clicktracking="off" target="_blank" href="https://ltk.app.link/FOOVWzL6NV" style="display: inline-block; color: black; text-transform: capitalize;">
            <img src="https://static.liketoknow.it/ltk-newsletter-content/2019-01-25/app-store-dl-android.png" style="width: 118px; height: 36px; margin: 0 5px; border: 0; box-shadow: none;" />
          </a>
        </td>
      </tr>
    </tbody>
  </table>`;
};

export { emailFooter };
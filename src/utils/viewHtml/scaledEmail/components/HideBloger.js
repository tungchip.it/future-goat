const HideBloger = dataBanner => {
    return `
<td width="33%" valign="bottom" align="center" style="width: 33%; padding: 2%">
  <table valign="top" cellspacing="0" style="width: 100%" cellpadding="5" border="0" align="center">
     <tbody style="width: 100%">
        <tr style="width: 100%">
           <td colspan="4" valign=" middle" align="center" width="100%" style="word-wrap: break-word" class="blogger_name_td">
               <h3 style="display: block;letter-spacing: 1px;text-transform: uppercase;font-weight: normal;font-family: Gotham, 'Gill Sans', sans-serif;text-align: center;font-size: 1.5vw;" class="blogger_name">                 
              </h3>
           </td>
        </tr>
        <tr>
           <td colspan="4" width="100%">
              <img src="https://rs-folder.s3.amazonaws.com/img/white-square.png" style="width:100%;" class="hero_img" width="100%">
           </td>
        </tr>
     </tbody>
     <tbody>
        <tr>
           <td width="25%" style="padding: 2%;">
              <img class="product_img" style="width:100%;" src="https://rs-folder.s3.amazonaws.com/img/white-square.png" width="100%">
           </td>
           <td width="25%" style="padding: 2%;">
              <img class="product_img" style="width:100%;" src="https://rs-folder.s3.amazonaws.com/img/white-square.png" width="100%">
           </td>
           <td width="25%" style="padding: 2%;">
              <img class="product_img" style="width:100%;" src="https://rs-folder.s3.amazonaws.com/img/white-square.png" width="100%">
           </td>
           <td width="25%" style="padding: 2%;">
              <img class="product_img" style="width:100%;" src="https://rs-folder.s3.amazonaws.com/img/white-square.png" width="100%">
           </td>
        </tr>
        <tr>
         <td width="25%" style="padding: 2%;">
            <img class="product_img" style="width:100%;" src="https://rs-folder.s3.amazonaws.com/img/white-square.png" width="100%">
         </td>
         <td width="25%" style="padding: 2%;">
            <img class="product_img" style="width:100%;" src="https://rs-folder.s3.amazonaws.com/img/white-square.png" width="100%">
         </td>
         <td width="25%" style="padding: 2%;">
            <img class="product_img" style="width:100%;" src="https://rs-folder.s3.amazonaws.com/img/white-square.png" width="100%">
         </td>
         <td width="25%" style="padding: 2%;">
            <img class="product_img" style="width:100%;" src="https://rs-folder.s3.amazonaws.com/img/white-square.png" width="100%">
         </td>
      </tr>
        <tr>
           <td colspan="4">
              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                 <tbody>
                    <tr>
                       <td></td>
                    </tr>
                 </tbody>
              </table>
           </td>
        </tr>
     </tbody>
  </table>
</td>`;
};

export { HideBloger };
const sale_banner = dataBanner => {
    return `
<table role="presentation" valign="top" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: 2rem auto 0;width: 100%;background-color: #fff;">
  <tbody>
    <tr width="100%">
      <td width="100%">
        <a clicktracking="off" href="${dataBanner.adv_link ? dataBanner.adv_link : ""}" target="_blank">
          <img style="width: 100%" src="${dataBanner.leaderBoardImage? dataBanner.leaderBoardImage: ''}" width="100%" />
        </a>
      </td>
    </tr>
  </tbody>
</table>`;
};

export { sale_banner };
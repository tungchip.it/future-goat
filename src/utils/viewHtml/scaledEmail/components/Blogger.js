import store from "../../../../store/index";
import _ from "lodash";
import { blogger_name } from "./blogger_name";
import { CTA } from "./CTA";
import { product_images } from "./product_images";
import { hero_image } from "./hero_image";

const Blogger = top_pick => {
  let GETTER_DATA_BLOGGER_NAME_ENABLE =
    store.getters.GETTER_DATA_BLOGGER_NAME_ENABLE;
  let GETTER_DATA_PRODUCT_OF_ROW = store.getters.GETTER_DATA_PRODUCT_OF_ROW;
  let GETTER_DATA_BLOGGER_CTA_ENABLE =
    store.getters.GETTER_DATA_BLOGGER_CTA_ENABLE;

  let chuckListImage = () => {
    let arr = ``;
    let newArr = [];
    if (GETTER_DATA_PRODUCT_OF_ROW > 4) {
      let dataTopPick = _.chunk(
        top_pick.products,
        GETTER_DATA_PRODUCT_OF_ROW / 2
      );
      newArr = [dataTopPick[0], dataTopPick[1]];
    } else {
      let dataTopPick = _.chunk(top_pick.products, GETTER_DATA_PRODUCT_OF_ROW);
      newArr = [dataTopPick[0]];
    }
    newArr.forEach((e, i) => {
      arr += `<tr>
            ${product_images(e, GETTER_DATA_PRODUCT_OF_ROW)}
          </tr>`;
    });
    return arr;
  };

  return `
  <td  style="width:33.33%;padding: 0 1%;" valign="bottom" align="center">
    <table valign="top " cellspacing="0 " style="width: 100%" cellpadding="5" border="0 " align="center ">
        <tbody style="width: 100%">
            ${GETTER_DATA_BLOGGER_NAME_ENABLE ? blogger_name(top_pick) : ""}
            ${hero_image(top_pick)}
            ${chuckListImage()}
            ${GETTER_DATA_BLOGGER_CTA_ENABLE ? CTA(top_pick) : ""}
        </tbody>
    </table>
  </td>
`;
};

export { Blogger };

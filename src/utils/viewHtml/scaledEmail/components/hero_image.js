let hero_image = top_pick => {
    return `
    <tr>
        <td colspan="4" width="100%">
          <a clicktracking="off" class="gram_link" style="width: 100%; display: block"  href="${top_pick.share_url? top_pick.share_url +"?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert": "#"}" target="_blank">
            <img src="${top_pick.hero_image}?auto=format&fm=webp&q=80&dpr=1" style="width: 100%" class="hero_img">
          </a>
        </td>
    </tr>`;
};
export { hero_image };
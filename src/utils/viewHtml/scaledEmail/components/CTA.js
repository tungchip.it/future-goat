let CTA = top_pick => {
  return `<tr>
            <td colspan="4">
              <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <td>
                      <a clicktracking="off" href="${top_pick.share_url? top_pick.share_url+"?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert": "#"}" target="_blank" style="margin:10px auto 15px;display: block;font-family: Didot, 'Palatino Linotype',Palatino, serif;text-align: center;border: 1px solid #000000;font-size: 2vw;background-color: #ffffff;color: #000000;font-style: italic;padding: 10px 0;text-decoration: none;" class="blogger_cta">
                      ${top_pick.shopthelook}
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>`;
};

export { CTA };

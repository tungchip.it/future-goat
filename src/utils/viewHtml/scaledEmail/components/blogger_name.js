const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-/]))?/;

const blogger_name = top_pick => {
  let checkUrl = top_pick => {
    let href = "";
    if (!regexp.test(top_pick.blog_url)) {
      href =
        top_pick.blog_url != null
          ? `href="http://${top_pick.blog_url}?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert"`
          : "";
    } else {
      href = `href="${top_pick.blog_url}?utm_source=newsletter&utm_medium=email&utm_campaign=trend-alert"`;
    }
    return href;
  };

  return `<tr style="width: 100%">
            <td colspan="4" valign="bottom" align="center" width="100%" style="word-wrap: break-word" class="blogger_name_td">
              <h3 style="display: block;letter-spacing: 1px;text-transform: uppercase;font-weight: normal;font-family: Gotham, 'Gill Sans', sans-serif;text-align: center;font-size: 2vw; word-break: break-all;word-wrap: break-word;" class="blogger_name">
                <a clicktracking="off" ${checkUrl(top_pick)} target="_blank" style="text-decoration: none;text-align: center;color: black;">${top_pick.blogger_name}</a>
              </h3>
            </td>
          </tr>`;
};

export { blogger_name };

import axios from "axios";
import { ModalBus } from "../../eventBus";
import Alert from "@/components/Alert";

// const proxy = "https://cors-anywhere.herokuapp.com/";
const proxy = "https://cors-anywhere.herokuapp.com/";
// const baseDomain = "https://k9azst1ls8.execute-api.us-east-1.amazonaws.com";
// const apiKey = "UThcV16IOK8ZOo9bBG4Dp9LxjYyVAibT8wF5DbOv";
const baseDomain = "https://juw4sbiiu8.execute-api.us-east-1.amazonaws.com/v0";
const apiKey = "Y4zKTZNyzrJr7PSLcbJt1wXkt1fiiR44VOllCjdh";

const baseURL = `${proxy + baseDomain}`;

// create a new axios instance
const instance = axios.create({
    baseURL,
    headers: {
        "Content-Type": "application/json",
        "x-api-key": apiKey
            // "x-platform": "web"
            //   "redirect_uri": "https://localhost:8888"

    }
    // headers: {
    //   "platform": "web",
    //   "redirect_uri": "https://localhost:8888"
    // }
});

// Add a request interceptor
instance.interceptors.request.use(
    config => {
        const authCode = localStorage.getItem("authCode");
        const refreshToken = localStorage.getItem("refreshToken");
        const idToken = localStorage.getItem("userToken");
        // console.log("x-auth-code: ", authCode);
        if (authCode) {
            config.headers["auth_code"] = authCode;
        }
        if (refreshToken) config.headers["refresh_token"] = refreshToken;
        if (idToken) config.headers["id_token"] = idToken;
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);

//Add a response interceptor
instance.interceptors.response.use(
    response => {
        return response;
    },
    error => {
        const originalRequest = error.config;
        console.log("error response status: ", error.response.status);

        let messages = "";

        switch (error.response.status) {
            case 500:
                messages = "We got 500 error code from API's endpoint.";
                break;
            case 502:
                messages = "We got 502 error code from API's endpoint.";
                break;
            default:
                messages = "Sorry, there is no available information for this request";
                break;
        }
        const props = {
            type: "error",
            text: messages
        };
        ModalBus.$emit("open", {
            component: Alert,
            title: "Error",
            props
        });
        return Promise.reject(error);
    }
);

export default instance;
import axios from "axios";
import { ModalBus } from "../../eventBus";
import Alert from "@/components/Alert";

const baseDomain = "https://juw4sbiiu8.execute-api.us-east-1.amazonaws.com/v0";
const proxy = "https://cors-anywhere.herokuapp.com/";
const apiKey = "Y4zKTZNyzrJr7PSLcbJt1wXkt1fiiR44VOllCjdh";

// create a new axios instance

const headers = {
  "Content-Type": "application/json",
  "x-api-key": apiKey
};

const baseURL = `${baseDomain}`;
// create a new axios instance
const instance = axios.create({
  baseURL,
  headers
});

//Add a response interceptor
instance.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    let text = "";
    switch (error.response.status) {
      case 500:
        text = "The server returned 500 again!";
        break;
      case 400:
        text = "Sorry, your request is invalid.";
        break;
      default:
        text = "Sorry, there is no available information for this request.";
        break;
    }
    const props = {
      type: "error",
      text: text
    };
    ModalBus.$emit("open", {
      component: Alert,
      title: "An error has occured",
      props
    });

    return Promise.reject(error);
  }
);

export default instance;

import LTKClient from "./LTKAxios";
import RSClient from "./RewardStyleAxios";
import Login from "./Clients/LoginClient";
import CropImage from "./Clients/LTKCropImage";

const login = "/login";
const ltks_v1 = "api/v1/ltks/";
const ltks_v2 = "api/ltk/v2/ltks/?hashes[]=";
const crop = "/imaging";
const influencer = "/influencer";

export default {
    loginPost(payload) {
        return Login.post(`${login}`, payload);
    },
    ltks(hash) {
        return LTKClient.get(`${ltks_v1 + hash}`);
    },
    rewardStyle(hash) {
        return RSClient.get(`${ltks_v2 + hash}`);
    },
    influencerPost(payload) {
        return Login.post(`${influencer}`, payload);
    },
    CropImage(payload) {
        return CropImage.post(`${crop}`, payload);
    }
};
import RiaServiceEndpoint from "./RiaServiceRepository";

const repositories = {
  riaServiceEnpoint: RiaServiceEndpoint
};
export default {
  get: name => repositories[name]
};

// Create rs-font containing the SVG icons in
// NJK CSS template is used to generate glyph classes
// Any css changes to the font should be added to the NJK template

const fs = require('fs')
const path = require('path')
const dir = path.resolve(__dirname)
const webfont = require('webfont').default

webfont({
  files: `${dir}/glyphs/*.svg`,
  fontName: 'rsfont',
  normalize: true,
  centerHorizontally: true,
  fixedWidth: false,
  fontHeight: 10000, // set to a large height so that icons render at high res during the conversion to prevent distortion
  template: `${dir}/fonts/rsfont.css.njk`
})
  .then((result) => {
    fs.writeFileSync(`${dir}/fonts/rsfont.css`, result.template)
    fs.writeFileSync(`${dir}/fonts/rsfont.svg`, result.svg)
    fs.writeFileSync(`${dir}/fonts/rsfont.ttf`, result.ttf)
    fs.writeFileSync(`${dir}/fonts/rsfont.eot`, result.eot)
    fs.writeFileSync(`${dir}/fonts/rsfont.woff`, result.woff)
    fs.writeFileSync(`${dir}/fonts/rsfont.woff2`, result.woff2)
  })
  .catch((error) => {
    throw error
  })

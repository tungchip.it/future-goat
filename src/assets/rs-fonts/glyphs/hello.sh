#!/usr/bin/env bash
for file in $(ls -1 *.svg); do
  contents=$(xmlstarlet ed -d '//*[name()="svg"]/@width' $file | xmlstarlet ed -d '//*[name()="svg"]/@height')
  echo "$contents" > $file
done
